import requests
import concurrent.futures
import random


NB_THREADS=5
NB_OPERATIONS=50

def test_concurrent_write_only(tmp_collection):
    with concurrent.futures.ThreadPoolExecutor(max_workers=NB_THREADS) as executor:
        executor.map(thread_write_only, range(NB_THREADS))

    for index in range(5):
        (id, value) = compose_kv_pair(index)
        r = requests.get(f"http://localhost:8080/q/tmp/{id}")
        assert r.status_code == 200
        assert r.text == value

def test_concurrent_read_write(tmp_collection):
    with concurrent.futures.ThreadPoolExecutor(max_workers=NB_THREADS) as executor:
        executor.map(thread_read_write, range(NB_THREADS))

    for index in range(5):
        (id, value) = compose_kv_pair(index)
        r = requests.get(f"http://localhost:8080/q/tmp/{id}")
        assert r.status_code == 200
        assert r.text == value

def test_concurrent_read_write_delete(tmp_collection):
    with concurrent.futures.ThreadPoolExecutor(max_workers=NB_THREADS) as executor:
        executor.map(thread_read_write, range(NB_THREADS))

    for index in range(5):
        (id, value) = compose_kv_pair(index)
        r = requests.get(f"http://localhost:8080/q/tmp/{id}")

        assert r.status_code == 200 or r.status_code == 404
        if r.status_code == 200:
            assert r.text == value


def thread_write_only(name):
    for _ in range(NB_OPERATIONS):
        id_index = random.randint(0, 4)

        update_operation(name, id_index)

def thread_read_write(name):
    operations = {
        0: read_operation,
        1: read_operation,
        2: update_operation,
    }
    for _ in range(NB_OPERATIONS):
        id_index = random.randint(0, 4)
        op_index = random.randint(0, 2)

        operations[op_index](name, id_index)

def thread_read_write_delete(name):
    operations = {
        0: read_operation,
        1: read_operation,
        2: read_operation,
        3: update_operation,
        4: update_operation,
        5: delete_operation,
    }
    for _ in range(NB_OPERATIONS):
        id_index = random.randint(0, 4)
        op_index = random.randint(0, 5)

        operations[op_index](name, id_index)

def read_operation(name, index):
    (id, _) = compose_kv_pair(index)
    headers = {"user-agent": f"pytest/operation_thread_{name}"}
    url = f"http://localhost:8080/q/tmp/{id}"

    requests.get(url, headers=headers)

def update_operation(name, index):
    (id, value) = compose_kv_pair(index)
    headers = {"user-agent": f"pytest/operation_thread_{name}"}
    url = f"http://localhost:8080/q/tmp/{id}/{value}"

    requests.put(url, headers=headers)

def delete_operation(name, index):
    (id, _) = compose_kv_pair(index)
    headers = {"user-agent": f"pytest/operation_thread_{name}"}
    url = f"http://localhost:8080/q/tmp/{id}"

    requests.delete(url, headers=headers)

def compose_kv_pair(index):
    return (f"id_{index}", f"value_{index}")
