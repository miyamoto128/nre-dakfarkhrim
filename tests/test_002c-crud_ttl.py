import requests
import time


def test_modify_value_with_ttl(tmp_collection):
    url = "http://localhost:8080/q/tmp/id_2/expiring_value"
    headers = {
        "Content-Time-To-Live": "1",
        "User-Agent": "my-app/0.0.1",
    }
    r = requests.put(url, headers=headers)
    assert r.status_code == 204
    assert r.headers['Content-Time-To-Live'] == "1"

    r = requests.get("http://localhost:8080/q/tmp/id_2")
    assert r.status_code == 200
    assert r.headers['Content-Time-To-Live'] == "0"
    assert r.text == "expiring_value"

    time.sleep(1)

    r = requests.get("http://localhost:8080/q/tmp/id_2")
    assert r.status_code == 404


def test_modify_value_no_ttl(tmp_collection):
    r = requests.put("http://localhost:8080/q/tmp/id_2/expiring_value")
    assert r.status_code == 204
    assert r.headers['Content-Time-To-Live'] == "18446744073709551615"

    r = requests.get("http://localhost:8080/q/tmp/id_2")
    assert r.status_code == 200
    assert r.headers['Content-Time-To-Live'] == "18446744073709551615"

    time.sleep(1)

    r = requests.get("http://localhost:8080/q/tmp/id_2")
    assert r.status_code == 200
    assert r.headers['Content-Time-To-Live'] == "18446744073709551615"
