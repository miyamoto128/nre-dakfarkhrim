import requests
import json


def test_describe(tmp_collection):
    r = requests.get("http://localhost:8080/describe")
    assert r.status_code == 200
    assert r.headers['Content-Type'] == "application/json"

    data = r.json()
    assert data["count"] >= 1
    assert "tmp" in data["collections"]

def test_describe_collection_tmp(tmp_collection):
    r = requests.get("http://localhost:8080/describe/tmp")
    assert r.status_code == 200
    assert r.headers['Content-Type'] == "application/json"

    data = r.json()
    assert data["count"] == 5
    assert data["first_id"] == "id_0"
    assert data["last_id"] == "id_4"

def test_describe_empty_collection():
    r = requests.get("http://localhost:8080/describe")
    assert r.status_code == 200
    assert r.headers['Content-Type'] == "application/json"

    data = r.json()
    assert not "tmp" in data["collections"]

    r = requests.get("http://localhost:8080/describe/tmp")
    assert r.status_code == 404