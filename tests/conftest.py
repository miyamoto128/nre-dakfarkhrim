import requests
import pytest


@pytest.fixture
def tmp_collection():
    requests.delete("http://localhost:8080/q/tmp/new_id")

    for i in range(5):
        requests.put(f"http://localhost:8080/q/tmp/id_{i}/value_{i}")

    yield

    for i in range(5):
        requests.delete(f"http://localhost:8080/q/tmp/id_{i}")

    requests.delete("http://localhost:8080/q/tmp/new_id")


@pytest.fixture
def tmp_blob_collection():
    requests.delete("http://localhost:8080/q/tmp-blob/1_pixel_gif");
    requests.delete("http://localhost:8080/q/tmp-blob/bytes")
    yield
    requests.delete("http://localhost:8080/q/tmp-blob/bytes")
    requests.delete("http://localhost:8080/q/tmp-blob/1_pixel_gif");
