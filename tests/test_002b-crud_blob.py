import requests
import os


def test_add_tiny_gif(tmp_blob_collection):
    TINY_GIF = b"\x47\x49\x46\x38\x39\x61\x01\x00\x01\x00\x80\x00\x00\xff\xff\xff\x00\x00\x00\x21\xf9\x04\x01\x00\x00\x00\x00\x2c\x00\x00\x00\x00\x01\x00\x01\x00\x00\x02\x02\x44\x01\x00\x3b"

    r = requests.put(
        url="http://localhost:8080/q/tmp-blob/1_pixel_gif",
        data=TINY_GIF,
        headers={"Content-Type": "image/gif"},
    )
    assert r.status_code == 201

    r = requests.get("http://localhost:8080/q/tmp-blob/1_pixel_gif");
    assert r.status_code == 200
    assert r.headers['Content-Length'] == "43"
    assert r.headers['Content-Type'] == "image/gif"
    assert r.content == TINY_GIF

def test_add_blob(tmp_blob_collection):
    bytes = os.urandom(100_000_000)
    r = requests.put(
        url="http://localhost:8080/q/tmp-blob/bytes",
        data=bytes,
    )
    assert r.status_code == 201

    r = requests.get("http://localhost:8080/q/tmp-blob/bytes")
    assert r.status_code == 200
    assert r.headers['Content-Length'] == "100000000"
    assert r.headers['Content-Type'] == "application/octet-stream"
    assert r.content == bytes