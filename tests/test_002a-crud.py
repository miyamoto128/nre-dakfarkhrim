import requests


def test_lookup_ok(tmp_collection):
    r = requests.get("http://localhost:8080/q/tmp/id_1")
    assert r.status_code == 200
    assert r.text == "value_1"

def test_lookup_none(tmp_collection):
    r = requests.get("http://localhost:8080/q/tmp/unknown")
    assert r.status_code == 404

def test_add_value(tmp_collection):
    r = requests.get("http://localhost:8080/q/tmp/new_id")
    assert r.status_code == 404

    r = requests.put("http://localhost:8080/q/tmp/new_id/new_value")
    assert r.status_code == 201

    r = requests.get("http://localhost:8080/q/tmp/new_id")
    assert r.status_code == 200
    assert r.text == "new_value"

def test_modify_value(tmp_collection):
    r = requests.get("http://localhost:8080/q/tmp/id_1")
    assert r.status_code == 200
    assert r.text == "value_1"

    r = requests.put("http://localhost:8080/q/tmp/id_1/new_value")
    assert r.status_code == 204

    r = requests.get("http://localhost:8080/q/tmp/id_1")
    assert r.status_code == 200
    assert r.text == "new_value"

def test_remove_value(tmp_collection):
    r = requests.get("http://localhost:8080/q/tmp/id_1")
    assert r.status_code == 200

    r = requests.delete("http://localhost:8080/q/tmp/id_1")
    assert r.status_code == 204

    r = requests.get("http://localhost:8080/q/tmp/id_1")
    assert r.status_code == 404
