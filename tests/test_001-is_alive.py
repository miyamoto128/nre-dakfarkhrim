import requests


def test_is_alive():
    r = requests.get("http://localhost:8080/info/is_alive")
    assert r.status_code == 200
    assert r.text == "UP"

def test_https():
    r = requests.get("https://localhost:8443/info/is_alive", verify=False)
    assert r.status_code == 200
    assert r.text == "UP"
