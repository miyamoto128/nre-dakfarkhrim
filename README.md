Non-regression tests for Dakfarkhrim project (https://gitlab.com/miyamoto128/dakfarkhrim).

## Usage

Get a local running Dakfarkhrim.
Then, to execute the tests:
```
pytest
``` 

## Getting start

This project uses poetry as dependency management and packaging tool.

To install poetry, follow the official guideline: https://python-poetry.org/docs/#installation.

Then, upgrade the dependencies:
```
poetry install
```

Execute the tests:
```
pytest
``` 